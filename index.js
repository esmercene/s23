// console.log('Hello World')


// -----------No. 1-----------
let trainer = {
	
		name: "Ash Ketchum",
		age: 10,
		pokemon: ["Pikachu","Charizard","Squirtle","Bulbasur"],
		friends: {
			hoenn: ["May","Max"],
			kanto: ["Brock","Misty"],
			},	
// console.log("Result of dot notation: ")

talk: function (){
		console.log("Pikachu! I choose you!")
}} 
console.log(trainer);
console.log("Result of dot notation:")
console.log(trainer.name);
console.log("Result of square notation:")
console.log(trainer.pokemon);
console.log("Result of talk method");
trainer.talk()









// -----------No. 2-----------
class Pokemon {
	
	constructor(name, level, health, attack, tackle){
		this.name = name;
		this.level = level;
		this. health = health;
		this.attack = attack;

	}
}

let nameA = new Pokemon("Pikachu", "12", "24","12");
let nameB = new Pokemon("Geodude", "8", "16","8");
let nameC = new Pokemon("Mewtwo", "100", "200","100");
console.log(nameA);
console.log(nameB);
console.log(nameC);







// ---------No. 3 ---------------
function Pokemon2(name, level){
	//properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	//methods
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name + "'s health is now reduced to " + Number(target.health - this.attack));
	}

	this.faint = function(){
		console.log(this.name + " fainted.")
	}
}

let pikachu = new Pokemon2("Pikachu", 12,24,12);
let geodude = new Pokemon2("Geodude", 8,16,8);
let mewtwo = new Pokemon2("Mewtwo", 100,200.100);
// pikachu.talk()

geodude.tackle(pikachu)
console.log(nameA);
mewtwo.tackle(geodude)
geodude.faint()
console.log(geodude);








